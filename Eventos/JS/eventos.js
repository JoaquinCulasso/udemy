$(document).ready(function () {
    //MouseOver MouseOut
    var caja = $("#caja");

    // caja.mouseover(function () {
    //     $(this).css("background", "red");
    // });

    // caja.mouseout(function () {
    //     $(this).css("background", "green");
    // })


    function cambiaRojo() {
        $(this).css("background", "red");
    }
    function cambiaVerde() {
        $(this).css("background", "green");
    }

    //hover      
    caja.hover(cambiaRojo, cambiaVerde);

    //click double-click
    caja.click(function () {
        $(this).css('background', 'black')
            .css('color', 'white');
    })

    caja.dblclick(function () {
        $(this).css('background', 'pink')
            .css('color', 'yellow');
    })

    //Focus Blur

    var nombre = $("#nombre");
    nombre.focus(function () {
        $(this).css("border", "2px solid green");
    })

    nombre.blur(function () {
        $(this).css("border", "2px solid transparent");

        $(".datos").text($(this).val()).show();
    })

    //MouseDown MouseUp
    var datos = $('.datos');
    datos.mousedown(function () {
        $(this).css('border-color', 'green');
    });
    datos.mouseup(function () {
        $(this).css('border-color', 'pink');
    });

    //MouseMove
    $(document).mousemove(function () {
        console.log('En X: ' + event.clientX);
        console.log('En Y: ' + event.clientY);
        $('.sigueme').css('left', event.clientX);
        $('.sigueme').css('top', event.clientY);
    })
});