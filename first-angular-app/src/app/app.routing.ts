//Importar modulos del router de Angular
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//Importar componentes
import { ZapatillaComponent } from "./zapatillas/zapatilla.component";
import { VideoGameComponent } from "./videogame/videogame.component";
import { CursosComponent } from "./cursos/cursos.component";
import { HomeComponent } from "./home/home.component";
import { ExternoComponent } from "./externo/externo.component";
import { ContactoComponent } from "./contacto/contacto.component";

//Array de rutas
const appRoutes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "zapatillas", component: ZapatillaComponent },
  { path: "videojuego", component: VideoGameComponent },
  { path: "cursos", component: CursosComponent },
  { path: "cursos/:nombre", component: CursosComponent },
  { path: "cursos/:nombre/:followers", component: CursosComponent },
  { path: "externo", component: ExternoComponent },
  { path: "contacto", component: ContactoComponent },
  { path: "**", component: HomeComponent }
];

//exportar el modulo del router
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
