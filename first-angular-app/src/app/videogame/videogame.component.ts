import { Component, OnInit, DoCheck, OnDestroy } from "@angular/core";

@Component({
  selector: "videogame",
  templateUrl: "./videogame.component.html"
})
export class VideoGameComponent implements OnInit, DoCheck, OnDestroy {
  public titulo: string;
  public listado: string;
  constructor() {
    this.titulo = "Componente de video juego";
    this.listado = "Listado de juegos";
    console.log("Se ha cargado el componente VideoGameComponent");
  }

  ngOnInit() {
    //Metodo que se ejecuta cuando carga el componente dsp del constructor
    console.log("Metodo OnInit ejecutandose!!");
  }

  ngDoCheck() {
    // Metodo que se ejecuta ante cambios en el codigo
    console.log("Metodo DoCheck ejecutado");
  }

  ngOnDestroy() {
    //Antes de eliminar la instancia de un componente que se ejecute algo
    console.log("Metodo OnDestroy ejecutado");
  }

  public setTitulo(title) {
    this.titulo = title;
  }

  public getTitulo(): string {
    return this.titulo;
  }

  public cambiarTitulo() {
    const valueTitle = document.getElementById("title");
    this.setTitulo(valueTitle);
  }
}
