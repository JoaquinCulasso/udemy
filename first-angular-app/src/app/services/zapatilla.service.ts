import { Injectable } from "@angular/core";
import { Zapatilla } from "../models/zapatilla";

@Injectable()
export class ZapatillaService {
  public zapatillas: Array<Zapatilla>;
  constructor() {
    this.zapatillas = [
      new Zapatilla("Booster", 4500, "Adidas", "negras", true),
      new Zapatilla("Nike Runner", 4000, "Nike", "gris", false),
      new Zapatilla("Nike Runner", 2950, "Nike", "blanca", false),
      new Zapatilla("Puma Training", 3000, "Puma", "Fluor", true),
      new Zapatilla("Puma Runner", 2000, "Puma", "Negro", true)
    ];
  }

  public getTexto() {
    console.log("Hola mundo desde un servicio");
  }

  public getZapatilla(): Array<Zapatilla> {
    return this.zapatillas;
  }
}
