import { Component, OnInit } from "@angular/core";
import { Zapatilla } from "../models/zapatilla";
import { ZapatillaService } from "../services/zapatilla.service";

@Component({
  selector: "zapatilla",
  templateUrl: "./zapatilla.component.html",
  providers: [ZapatillaService]
})
export class ZapatillaComponent implements OnInit {
  public titulo: string;
  public zapatillas: Array<Zapatilla>;
  public marcas: string[];
  public color: string;
  public miMarca: string;
  public nuevaMarca: string;

  constructor(private _zapatillaService: ZapatillaService) {
    console.log("Se ha cargado el componente ZapatillaComponent");
    this.color = "red";
    this.marcas = new Array();
    this.titulo = "componente de zapatilla";
  }

  ngOnInit() {
    this.zapatillas = this._zapatillaService.getZapatilla();
    this.getMarcas();
    this._zapatillaService.getTexto();
    // console.log(this.zapatillas);
  }

  public getTitulo(): string {
    return this.titulo;
  }

  public getMarcas() {
    this.zapatillas.forEach((value, index) => {
      // console.log(index);
      if (this.marcas.indexOf(value.marca) < 0) {
        this.marcas.push(value.marca);
      }
    });
    console.log(this.marcas);
  }

  public getMarca() {
    alert(this.miMarca);
  }

  public addMarca() {
    this.marcas.push(this.nuevaMarca);
  }

  public deleteMarca(index) {
    // delete this.marcas[index];
    this.marcas.splice(index, 1);
  }

  public onBlur() {
    console.log("has salido del input");
  }

  public mostrarPalabra() {
    alert(this.miMarca);
  }
}
