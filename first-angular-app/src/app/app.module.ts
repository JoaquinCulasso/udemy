import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { routing, appRoutingProviders } from "./app.routing";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { VideoGameComponent } from "./videogame/videogame.component";
import { ZapatillaComponent } from "./zapatillas/zapatilla.component";
import { CursosComponent } from "./cursos/cursos.component";
import { HomeComponent } from "./home/home.component";
import { ExternoComponent } from "./externo/externo.component";
import { CalculadoraPipe } from "./pipes/calculadora.pipe";
import { ContactoComponent } from './contacto/contacto.component';

@NgModule({
  declarations: [ //directivas
    AppComponent,
    VideoGameComponent,
    ZapatillaComponent,
    CursosComponent,
    HomeComponent,
    ExternoComponent,
    CalculadoraPipe,
    ContactoComponent
  ],
  imports: [BrowserModule, FormsModule, routing, HttpClientModule], //modelos
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule {}
