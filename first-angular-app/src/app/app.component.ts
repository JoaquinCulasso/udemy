import { Component } from "@angular/core";
import { Configuracion } from "./models/configuracion";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  public title: string = "first-angular-app";
  public descripcion: string;
  public VideoGame: boolean = true;

  constructor() {
    this.title = Configuracion.titulo;
    this.descripcion = Configuracion.descripcion;
  }

  public getStatusVideoGame(): boolean {
    return this.VideoGame;
  }

  public setStatusGame(valor) {
    this.VideoGame = valor;
  }
}
