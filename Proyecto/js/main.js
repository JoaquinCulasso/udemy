$(document).ready(function() {
  //Slider
  if (window.location.href.indexOf("index") > -1) {
    $(".bxslider").bxSlider({
      mode: "fade",
      captions: true,
      slideWidth: 865
    });
  }

  //Posts
  if (window.location.href.indexOf("index") > -1) {
    var posts = [
      {
        title: "Prueba de titulo 1",
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      sem nisl, venenatis dapibus nunc sed, rutrum vulputate nisi. Duis
      pharetra orci leo, et ultricies nibh vestibulum in. Sed eu
      vehicula ex, id rutrum nisi. Nam volutpat, libero nec rhoncus
      tristique, mauris erat cursus velit, viverra semper libero mi ut
      est. Etiam tempor dolor quis elit cursus, sollicitudin hendrerit
      lorem congue. Praesent laoreet, eros ultrices sodales lobortis,
      nunc lacus sodales nisl, id pretium lectus tortor sed augue. Donec
      mollis neque velit, sed tincidunt ipsum commodo ut. Vestibulum
      ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia Curae; In nec mauris vitae elit auctor suscipit. In tempor
      sollicitudin sagittis. Mauris eleifend dolor ipsum, ac euismod
      ante luctus vel. In pellentesque odio ipsum, eget pretium justo
      tincidunt quis. Curabitur vitae massa accumsan nisi pellentesque
      fringilla ut vitae nisi. Etiam aliquet auctor odio, at elementum
      lectus lacinia nec.`
      },
      {
        title: "Prueba de titulo 2",
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      sem nisl, venenatis dapibus nunc sed, rutrum vulputate nisi. Duis
      pharetra orci leo, et ultricies nibh vestibulum in. Sed eu
      vehicula ex, id rutrum nisi. Nam volutpat, libero nec rhoncus
      tristique, mauris erat cursus velit, viverra semper libero mi ut
      est. Etiam tempor dolor quis elit cursus, sollicitudin hendrerit
      lorem congue. Praesent laoreet, eros ultrices sodales lobortis,
      nunc lacus sodales nisl, id pretium lectus tortor sed augue. Donec
      mollis neque velit, sed tincidunt ipsum commodo ut. Vestibulum
      ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia Curae; In nec mauris vitae elit auctor suscipit. In tempor
      sollicitudin sagittis. Mauris eleifend dolor ipsum, ac euismod
      ante luctus vel. In pellentesque odio ipsum, eget pretium justo
      tincidunt quis. Curabitur vitae massa accumsan nisi pellentesque
      fringilla ut vitae nisi. Etiam aliquet auctor odio, at elementum
      lectus lacinia nec.`
      },
      {
        title: "Prueba de titulo 3",
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      sem nisl, venenatis dapibus nunc sed, rutrum vulputate nisi. Duis
      pharetra orci leo, et ultricies nibh vestibulum in. Sed eu
      vehicula ex, id rutrum nisi. Nam volutpat, libero nec rhoncus
      tristique, mauris erat cursus velit, viverra semper libero mi ut
      est. Etiam tempor dolor quis elit cursus, sollicitudin hendrerit
      lorem congue. Praesent laoreet, eros ultrices sodales lobortis,
      nunc lacus sodales nisl, id pretium lectus tortor sed augue. Donec
      mollis neque velit, sed tincidunt ipsum commodo ut. Vestibulum
      ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia Curae; In nec mauris vitae elit auctor suscipit. In tempor
      sollicitudin sagittis. Mauris eleifend dolor ipsum, ac euismod
      ante luctus vel. In pellentesque odio ipsum, eget pretium justo
      tincidunt quis. Curabitur vitae massa accumsan nisi pellentesque
      fringilla ut vitae nisi. Etiam aliquet auctor odio, at elementum
      lectus lacinia nec.`
      },
      {
        title: "Prueba de titulo 4",
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      sem nisl, venenatis dapibus nunc sed, rutrum vulputate nisi. Duis
      pharetra orci leo, et ultricies nibh vestibulum in. Sed eu
      vehicula ex, id rutrum nisi. Nam volutpat, libero nec rhoncus
      tristique, mauris erat cursus velit, viverra semper libero mi ut
      est. Etiam tempor dolor quis elit cursus, sollicitudin hendrerit
      lorem congue. Praesent laoreet, eros ultrices sodales lobortis,
      nunc lacus sodales nisl, id pretium lectus tortor sed augue. Donec
      mollis neque velit, sed tincidunt ipsum commodo ut. Vestibulum
      ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia Curae; In nec mauris vitae elit auctor suscipit. In tempor
      sollicitudin sagittis. Mauris eleifend dolor ipsum, ac euismod
      ante luctus vel. In pellentesque odio ipsum, eget pretium justo
      tincidunt quis. Curabitur vitae massa accumsan nisi pellentesque
      fringilla ut vitae nisi. Etiam aliquet auctor odio, at elementum
      lectus lacinia nec.`
      },
      {
        title: "Prueba de titulo 5",
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
      sem nisl, venenatis dapibus nunc sed, rutrum vulputate nisi. Duis
      pharetra orci leo, et ultricies nibh vestibulum in. Sed eu
      vehicula ex, id rutrum nisi. Nam volutpat, libero nec rhoncus
      tristique, mauris erat cursus velit, viverra semper libero mi ut
      est. Etiam tempor dolor quis elit cursus, sollicitudin hendrerit
      lorem congue. Praesent laoreet, eros ultrices sodales lobortis,
      nunc lacus sodales nisl, id pretium lectus tortor sed augue. Donec
      mollis neque velit, sed tincidunt ipsum commodo ut. Vestibulum
      ante ipsum primis in faucibus orci luctus et ultrices posuere
      cubilia Curae; In nec mauris vitae elit auctor suscipit. In tempor
      sollicitudin sagittis. Mauris eleifend dolor ipsum, ac euismod
      ante luctus vel. In pellentesque odio ipsum, eget pretium justo
      tincidunt quis. Curabitur vitae massa accumsan nisi pellentesque
      fringilla ut vitae nisi. Etiam aliquet auctor odio, at elementum
      lectus lacinia nec.`
      }
    ];

    //   console.log(posts);

    posts.forEach(item => {
      let post = `
    <article class="post">
            <h2>${item.title}</h2>
            <span class="date">${item.date}</span>
            <p>
              ${item.content}
            </p>
            <a href="#" class="button-more">Leer más</a>
          </article>
    `;

      // console.log(post);

      $("#posts").append(post);
    });
  }
  //Selector de tema
  let theme = $("#theme");
  $("#to-green").click(function() {
    theme.attr("href", "css/green.css");
  });
  $("#to-blue").click(function() {
    theme.attr("href", "css/blue.css");
  });
  $("#to-red").click(function() {
    theme.attr("href", "css/red.css");
  });

  //Scroll arriba de la web
  $(".subir").click(function(e) {
    e.preventDefault();
    $("hmtl, body").animate({
      scrollTop: 0
    });
    return false;
  });

  //login falso
  $("#login form").submit(function() {
    const form_name = $("#form_name").val();
    localStorage.setItem("form_name", form_name);
  });

  let form_name = localStorage.getItem("form_name");

  if (form_name != null && form_name != undefined) {
    const about_parrafo = $("#about p");
    $("#about p").html("Bienvenido " + form_name);
    about_parrafo.append("<a href='#' id='logout'>Cerrar Sesion</a>");

    $("#login").hide();

    $("#logout").click(function() {
      localStorage.clear();
      location.reload();
    });
  }

  //Acordeon
  if (window.location.href.indexOf("about") > -1) {
    $("#acordeon").accordion();
  }

  //Reloj
  if (window.location.href.indexOf("reloj") > -1) {
    setInterval(function() {
      const reloj = moment().format("hh:mm:ss");
      $("#reloj").html(reloj);
    }, 1000);
  }

  //Validate-form JQuery html-contact
  if (window.location.href.indexOf("contacto") > -1) {
    $("form input[name='date']").datepicker({
      dateFormat: "dd-mm-yy"
    });
    $.validate({
      lang: "es",
      errorMessagePosition: "top",
      scrollToTopOnError: true
    });
  }
});
