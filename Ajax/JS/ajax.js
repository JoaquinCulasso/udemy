$(document).ready(function () {

    // // Load
    const datos = $('#datos');
    // datos.load('https://reqres.in/');

    //GET
    $.get('https://reqres.in/api/users', { page: 2 }, function (response) {
        // console.log(response);
        response.data.forEach(element => {
            datos.append('<p>' + element.first_name + ' ' + element.last_name + '</p>');
        });
    });

    //POST

    let usuario = {
        first_name: 'Joaquin',
        last_name: 'Culasso'
    };

    $.post('https://reqres.in/api/users', usuario, function (response) {
        console.log(response);
    });

    const formulario = $('#formulario');



    formulario.submit(function (e) {
        e.preventDefault();

        let usuarioHTML = {
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val()
        };

        $.post(formulario.attr('action'), usuarioHTML, function (response) {
            console.log(response);
        });


        $.ajax({
            type: 'POST',
            url: formulario.attr('action'),
            data: usuarioHTML,
            beforeSend: function () {
                console.log('Enviando usuario');

            },
            succes: function (response) {
                console.log(response);
            },
            error: function () {
                console.log('A ocurrido un error')
            },
            timeout: 2000
        });

        return false;
    });
});