/**
* Fecth (ajax) y peticiones asincronas a servicios / api rest
*/
window.onload = function () {

    var div_usuarios = document.querySelector("#usuarios");
    var div_janet = document.querySelector("#janet");


    getUsuarios()
        .then(result => result.json())
        .then(users => {
            listadoUsuarios(users.data)
            return getJanet();
        })
        .then(data => data.json())
        .then(janet => {
            mostrarJanet(janet.data);

            return getInfo();
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error, "error en la peticiones");
        });

    function getUsuarios() {
        return fetch('https://reqres.in/api/users');
    }

    function getJanet() {
        return fetch('https://reqres.in/api/users/2');
    }

    function getInfo() {
        var profe = {
            nombre: 'joaquin',
            apellido: 'culasso',
            url: 'www.google.com.ar'
        };
        return new Promise((resolve, reject) => {
            var profesor_string = JSON.stringify(profe);

            if (typeof profesor_string != 'string') {
                return reject('error');
            }

            return resolve(profesor_string);
        });

    }

    function listadoUsuarios(usuarios) {
        usuarios.map((user, i) => {
            let nombre = document.createElement('h2');
            nombre.innerHTML = i + " " + user.first_name + " " + user.last_name;

            div_usuarios.appendChild(nombre);

            document.querySelector(".loading").style.display = 'none';
        });
    }

    function mostrarJanet(user) {

        let nombre = document.createElement('h4');
        let avatar = document.createElement("img");

        nombre.innerHTML = user.id + " " + user.first_name + " " + user.last_name;
        avatar.src = user.avatar;
        avatar.width = "100"

        div_janet.appendChild(nombre);
        div_janet.appendChild(avatar);

        document.querySelector("#janet .loading").style.display = 'none';
    }
}