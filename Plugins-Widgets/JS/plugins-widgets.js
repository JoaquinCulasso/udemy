$(document).ready(function() {

    // //tooltip
    $(document).tooltip();

    //dialog
    $('#lanzar-popup').click(function() {
        $('#popup').dialog();
    });

    //Datepicker
    $('#calendario').datepicker();

    //tabs
    $('#pestanias').tabs();
});