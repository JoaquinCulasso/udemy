$(document).ready(function() {
    const element = $('.element');

    //DRAGGABLE (mover elemento por la pag)
    element.draggable();

    //RESIZABLE (redimensionar un elemento en la pag)
    element.resizable();

    //SELECTABLE (seleccionar elementos)
    const list = $('#lista');

    list.selectable();

    //SORTABLE (ordernar los elementos de una lista)
    const list_ordered = $('#list-ordered');

    list_ordered.sortable();

    //DROPPABLE (soltar y arrastrar)
    const area = $('.area');
    const element_move = $('.element-move');

    element_move.draggable();
    area.droppable({
        drop: function() {
            console.log('has soltado algo dentro del area');
        }
    });

    //EFECTOS
    const boton = $('#mostrar');
    const caja_efectos = $('.caja-efectos');
    boton.click(function() {
        // caja_efectos.fadeToggle();
        // caja_efectos.toggle('explode');
        // caja_efectos.toggle('blind');
        // caja_efectos.toggle('slide');
        // caja_efectos.toggle('fold');
        // caja_efectos.toggle('drop');
        // caja_efectos.toggle('puff');
        // caja_efectos.toggle('scale')
        caja_efectos.toggle('shake')

    });
});