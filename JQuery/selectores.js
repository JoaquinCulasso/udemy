$(document).ready(function() {
    //Selector de ID
    var rojo = $("#rojo")
        .css("background", "red")
        .css("color", "white");

    var amarillo = $("#amarillo")
        .css('background', 'yellow')
        .css('color', 'green');

    var verde = $('#verde')
        .css('background', 'green')
        .css('color', 'white');
    console.log(rojo);
    console.log(amarillo);
    console.log(verde);

    //Selector de clases
    var zebra = $('.zebra')
        .css('padding', '2px');

    var sin_nada = $('.sin_nada');

    sin_nada.click(function() {
        sin_nada.addClass('zebra');
    });
    console.log(zebra);

    //Selector de etiquetas
    var parrafos = $('p');

    parrafos.click(function() {
        var parr = $(this);
        if (!parr.hasClass('grande')) {
            $(this).addClass('grande');
        } else {
            parr.removeClass('grande');
        }
    });

    //Selector de atributos
    $('[title = "Google"]').css('background', 'red');
    $('[title = "Facebook"]').css('background', 'yellow');
    $('[title = "Gmail"]').css('background', 'blue');

    //Otros selectores
    $('p, a').addClass('margen_superior');

});