$(document).ready(function () {
    var mostrar = $('#mostrar');
    var ocultar = $('#ocultar');
    var caja = $('#caja');

    caja.hide();

    mostrar.click(function () {
        caja.show();
    });
    ocultar.click(function () {
        caja.hide();
    });
});