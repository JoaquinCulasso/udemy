//String
var cadena = "Joaquin";
cadena = 136;
//number
var numero = 12;
//booleando
var verdadero_falso = true;
//any (cualquier valor)
var cualquiera = "hola";
//Arrays
var lenguajes = ["PHP", "Javascript", "Java"];
var years = [12, 13, 14, 15];
var aux = 12;
aux = "joa";
//let o var
var num1 = 10;
var num2 = 12;
if ((num1 = 10)) {
    var num1_1 = 55;
    var num2 = 75;
    console.log(num1_1, num2);
}
console.log(num1, num2);
console.log(cadena);
console.log(numero);
console.log(verdadero_falso);
console.log(cualquiera);
console.log(lenguajes);
console.log(years);
console.log(aux);
