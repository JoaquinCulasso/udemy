//String
let cadena: string | number = "Joaquin";
cadena = 136;

//number
let numero: number = 12;

//booleando
let verdadero_falso: boolean = true;

//any (cualquier valor)
let cualquiera: any = "hola";

//Arrays
var lenguajes: Array<String> = ["PHP", "Javascript", "Java"];

let years: number[] = [12, 13, 14, 15];

//Crear tipo de dato
type letraonum = string | number;

let aux: letraonum = 12;
aux = "joa";

//let o var
var num1 = 10;
var num2 = 12;

if ((num1 = 10)) {
  let num1 = 55;
  var num2 = 75;
  console.log(num1, num2);
}
console.log(num1, num2);

console.log(cadena);
console.log(numero);
console.log(verdadero_falso);
console.log(cualquiera);
console.log(lenguajes);
console.log(years);
console.log(aux);
