window.onload = function () {
    var formPeli = document.querySelector("#formpeliculas");
    formPeli.addEventListener('submit', function () {
        var peli = document.querySelector("#addpelicula").value;

        if (peli.length >= 1) {
            localStorage.setItem(peli, peli);
        }
    });

    var ul = document.querySelector("#pelis-list");

    for (const key in localStorage) {
        // console.log(localStorage[key]);

        if (typeof localStorage[key] == 'string') {
            var li = document.createElement('li');
            li.append(localStorage[key]);
            ul.append(li);
        }
    }

    var formPeliRemove = document.querySelector("#formBorrarPeliculas");
    formPeliRemove.addEventListener('submit', function () {
        var peliRemove = document.querySelector("#remove_pelicula").value;

        
            localStorage.removeItem(peliRemove);
     
    });
}

