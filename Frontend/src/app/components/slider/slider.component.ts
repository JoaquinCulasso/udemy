import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
declare var $: any;

@Component({
  selector: "slider",
  templateUrl: "./slider.component.html",
  styleUrls: ["./slider.component.css"]
})
export class SliderComponent implements OnInit {
  @Input() anchoSlider: number;
  @Input() etiquetas: boolean;
  @Output() getAutor = new EventEmitter();
  
  public autor: any;

  constructor() {
    this.autor = {
      nombre: "Joako",
      web: "es.asdasdasd.com",
      youtube: "SDERr"
    };
  }

  ngOnInit() {
    $("#logo").click(e => {
      e.preventDefault();
      $("header").css("background", "green");
    });

    $(".galeria").bxSlider({
      mode: "fade",
      captions: this.etiquetas,
      slideWidth: this.anchoSlider
    });
  }

  lanzar(evento) {
    console.log(evento);
    this.getAutor.emit(this.autor);
  }
}
