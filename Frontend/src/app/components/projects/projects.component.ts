import { Component, OnInit } from "@angular/core";
import { Project } from "../../models/project";
import { ProjectService } from "../../services/project.service";
import { Global } from "../../services/global";

@Component({
  selector: "app-projects",
  templateUrl: "./projects.component.html",
  styleUrls: ["./projects.component.css"],
  providers: [ProjectService]
})
export class ProjectsComponent implements OnInit {
  public projects: Project[];
  public url = Global.URL;

  constructor(private _projectService: ProjectService) {}

  ngOnInit() {
    this.getProject();
  }

  getProject() {
    this._projectService.getProject().subscribe(
      response => {
        if (response.projects) {
          this.projects = response.projects;
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}
