import { Component, OnInit, ViewChild } from "@angular/core";
declare var $: any;

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.css"]
})
export class ContactComponent implements OnInit {
  public widthSlider: number;
  public anchuraToSlider: number;
  public captions: boolean;
  public autor: any;

  @ViewChild("textos", {static: true}) texto;

  constructor() {
    this.captions = true;
  }

  ngOnInit() {
    var opcion_clasica = document.querySelector("#texto").innerHTML;
    console.log(this.texto.nativeElement.innerText);
  }

  cargarSlider() {
    this.anchuraToSlider = this.widthSlider;
  }

  resetearSlider() {
    this.anchuraToSlider = null;
  }

  conseguirAutor(event) {
    console.log(event);
    this.autor = event;
  }
}
