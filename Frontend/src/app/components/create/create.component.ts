import { Component, OnInit } from "@angular/core";
import { Project } from "../../models/project";
import { ProjectService } from "../../services/project.service";
import { UploadService } from "../../services/upload.service";
import { Global } from "../../services/global";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.css"],
  providers: [ProjectService, UploadService]
})
export class CreateComponent implements OnInit {
  public title: string;
  public project: Project;
  public status: string;
  public filesToUpload: Array<File>;
  public save_project;

  constructor(
    private _projectService: ProjectService,
    private _uploadService: UploadService
  ) {
    this.title = "Crear Proyecto";
    this.project = new Project("", "", "", "", 2019, "", "");
  }

  ngOnInit() {}

  onSubmit() {
    this._projectService.saveProject(this.project).subscribe(
      response => {
        if (response.project) {
          if (this.filesToUpload) {
            //Subo la imagen
            this._uploadService
              .makeFileRequest(
                Global.URL + "upload-image/" + response.project._id,
                [],
                this.filesToUpload,
                "image"
              )
              .then((result: any) => {
                this.save_project = result.projecUpdate;
                console.log(this.save_project);

                this.status = "success";
              });
          } else {
            this.save_project = response.projecUpdate;
            console.log(this.save_project);
            this.status = "success";
          }
        } else {
          this.status = "failed";
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }
}
