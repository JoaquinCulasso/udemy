import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Project } from "../models/project";
import { Global } from "./global";

@Injectable()
export class ProjectService {
  public URI: string;

  constructor(private _http: HttpClient) {
    this.URI = Global.URL;
  }

  testService() {
    return "Probando el servicio de angular";
  }

  saveProject(project: Project): Observable<any> {
    let params = JSON.stringify(project);
    let header = new HttpHeaders().set("Content-Type", "application/json");

    return this._http.post(this.URI + "save-project", params, {
      headers: header
    });
  }

  getProject(): Observable<any> {
    let header = new HttpHeaders().set("Content-Type", "application/json");

    return this._http.get(this.URI + "projects", { headers: header });
  }

  getProjectId(id): Observable<any> {
    let header = new HttpHeaders().set("Content-Type", "application/json");

    return this._http.get(this.URI + "project/" + id, { headers: header });
  }

  deleteProject(id): Observable<any> {
    let header = new HttpHeaders().set("Content-Type", "application/json");

    return this._http.delete(this.URI + "project/" + id, { headers: header });
  }

  updateProject(project): Observable<any> {
    let params = JSON.stringify(project);
    let header = new HttpHeaders().set("Content-Type", "application/json");

    return this._http.put(this.URI + "project/" + project._id , params, {
      headers: header
    });
  }
}
