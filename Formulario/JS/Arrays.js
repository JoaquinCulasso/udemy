/**
 * 1-Pida 6 nros pon pantalla y los meta en un array
 * 2-Tiene q mostrar el array en la pag y consola
 * 3-Ahi q ordenarlo
 * 4-Invertir su orden y mostrarlo
 * 5-Cuanto elementos tiene el array
 * 6-Busqueda de un valor ingresado por el usuario, si se encuentra y su indice.
 */

var count = 0;
var ingreso = prompt("Ingrese el valor que se guardara en el array, y escriba 'S' para salir");
var arreglo = [];

while (ingreso != "S") {
    ingreso = prompt("Ingrese el valor que se guardara en el array, y escriba 'S' para salir");
    arreglo.push(ingreso);
}

arreglo.pop();

document.write("Arreglo por pagina segun ingreso<ul>");
arreglo.forEach(element => {
    document.write("<li>" + element + "</li>");  //arreglo por pag
});
document.write("</ul>");

document.write("Arreglo por ordenado <ul>");
arreglo.sort();
arreglo.forEach(element => {
    document.write("<li>" + element + "</li>");  //arreglo por pag
});
document.write("</ul>");

document.write("Arreglo por invertido <ul>");
arreglo.reverse();
arreglo.forEach(element => {
    document.write("<li>" + element + "</li>");  //arreglo por pag
});
document.write("</ul>");
// console.log(arreglo); // arreglo por consola
// console.log(arreglo.sort()); // arreglo ordenado
// console.log(arreglo.reverse());//arreglo invertido