window.addEventListener('load', function () {
    console.log('DOM cargado');

    var formulario = this.document.querySelector("#formulario");
    var box_dashed = this.document.querySelector(".dashed");
    box_dashed.style.display = "none";

    formulario.addEventListener('submit', function () {
        console.log("Evento submit capturado");

        var nombre = document.querySelector("#nombre").value;
        var apellido = document.querySelector("#apellido").value;
        var edad = parseInt(document.querySelector("#edad").value);

        if(nombre.trim() == null || nombre.trim().length == 0){
            alert("Nombre no valido")
            return false;
        }
        if(apellido.trim() == null || apellido.trim().length == 0){
            alert("Apellido no valido")
            return false;
        }
        if(isNaN(edad)){
            alert("Edad no valido")
            return false;
        }


        box_dashed.style.display = "block";

        console.log(nombre, apellido, edad);
        datos_usuario = [nombre, apellido, edad];

        // for (const key in datos_usuario) {
        //     var parrafo = document.createElement("p");
        //     parrafo.append(datos_usuario[key]);
        //     box_dashed.append(parrafo);
        // }

        var p_nombre = document.querySelector("#p_nombre span");
        var p_apellido = document.querySelector("#p_apellido span");
        var p_edad = document.querySelector("#p_edad span");

        p_nombre.innerHTML = nombre;
        p_apellido.innerHTML = apellido;
        p_edad.innerHTML = edad;

        alert('Nombre: ' + nombre + '\n' +
            'Apellido: ' + apellido + '\n' +
            'Edad: ' + edad);
    });
}); 